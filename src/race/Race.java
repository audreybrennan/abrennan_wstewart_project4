package race;

import java.util.Random;

public class Race implements RaceInterface {
	
	Random rand = new Random();
	private char[] racetrack;
	private Animal[] racers;
	protected boolean isDone = false;
	

	/**
	 * Method that randomly creates a race track and randomly generates characters.
	 * precondition: length and numOfRacers must be chosen.
	 * postcondition: char[] array race track is generated with a chosen length and random terrain, racers are
	 * randomly instantiated and generated.
	 * @param length of the track.
	 * @param numOfRacers in the race.
	 */
	public void createRace(int length, int numOfRacers){
		//create track
		racetrack = new char[length];
		for(int i = 0; i < racetrack.length; i++) {
			int num = rand.nextInt(4)+1;
			if(num == 1) {
				racetrack[i] = '.';
			}
			else if(num == 2) {
				racetrack[i] = '#';
			}
			else if(num == 3) {
				racetrack[i] = 'O';
			}
			else if(num == 4) {
				racetrack[i] = '~';
			}
		}
		racetrack[racetrack.length-1] = '|';
		
		//instantiate and generate racers
		racers = new Animal[numOfRacers];
		
		for(int i = 0; i < numOfRacers; i++) {
			int num = rand.nextInt(3) + 1;
			if (num == 1) {
				racers[i] = new Monkey();
			}
			else if(num == 2) {
				racers[i] = new Ostrich();
			}
			else if(num == 3) {
				racers[i] = new Turtle();
			}
		}
	}
	
	/**
	 * returns the current racetrack.
	 * Precondition: the racetrack is generated.
	 * Postcondition: the racetrack that the racers used is returned.
	 * @returns char[] array racetrack.
	 */
	@Override
	public char[] getRacetrack() {
		return racetrack; 
	}

	/**
	 * Returns the name of a racer at a  index that is specified
	 * Precondition: the racerIndex is larger than or equal to 0 and less than length and racer exists at the chosen index.
	 * Postcondition: The string name of the racer at the chosen index is returned.
	 * @param the index of the chosen racer 
	 * @returns a string with the name of the racer at the chosen index
	 */
	@Override
	public String getRacerName(int racerIndex) {

		return racers[racerIndex].getName();
	}

	/**
	 * returns the position of the racer at the chosen index.
	 * Precondition: the racerIndex is larger than or equal to 0 and less than length and the racer exists at chosen index.
	 * Postcondition: an integer value between 0 and the length of the race track is returned.
	 * @param the index of the chosen racer.
	 * @returns an integer which specified the racer's current position.
	 */
	@Override
	public int getRacerPosition(int racerIndex) {
		return racers[racerIndex].getPosition();
		
	}
	
	/**
	 * Will be implemented soon
	 */
	public boolean getIsDone() {
		return isDone;
	}
	
	/**
	 * returns a boolean result which will be true if the racer at the chosen index won and false if the racer lost.
	 * Precondition: the racerIndex is larger than or equal to 0 and less than length.
	 * Postcondition: a boolean true or false is returned.
	 * @param the index of the chosen racer.
	 * @returns true if the racer won and false if the racer lost.
	 */
	@Override
	public boolean getRacerIsWinner(int racerIndex) {
		return getRacerPosition(racerIndex) == racetrack.length - 1;
	}
	
	/**
	 * Runs the move method on every animal in the array and then prints out the race if the race is over
	 * Preconditions: The racetrack has to exist and a list of racers also has to exist
	 */
	public void advanceOneTurn() {
		int count = 0;
		 while(count < racers.length) {
			 Animal racer = racers[count];
			 racer.move(racetrack);
			 if(racer.getPosition() == racetrack.length - 1) {
				 isDone = true;
			 }
			 count++;
		 }
		 if(isDone) {
		 printArray();
	}
	}
	/**
	 * Prints out the final set of arrays to show the results of the race
	 * Precondition: The racetrack has to exist and a list of racers also has to exist
	 */
	public void printArray() {
		int count = 0;
		int tracker = 0;
		while(count < racers.length) {
			Animal racer = racers[count];
			System.out.print(racer.getName() + "    ");
			System.out.print("(" + racer.getType() + ") ");
			int otherCount = 0;
			while(otherCount < racetrack.length) {
				if(racer.getPosition() == otherCount) {
					System.out.print("<" + racetrack[otherCount] + ">" + "     ");
				}
				else {
					System.out.print(racetrack[otherCount] + "    ");
				}
				otherCount++;
			}
			count++;	
			System.out.println(" ");
		}
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" ");
		count = 0;
		while(count < racers.length) {
			Animal racer = racers[count];
			if(racer.getPosition() == racetrack.length - 1) {
				System.out.print("Winner: " + racer.getName());
				System.out.println(" (" + racer.getType() + ") ");
			}
			count++;
		}
		
	}
}



