package race;
import java.util.Random;
public class Animal {
protected int maxSpeed;
protected int remainingSpeed;
protected int position;
protected Random randy;
protected String name;
protected String type;

public Animal(int speed, String typeOfAnimal) {
	randy = new Random();
	maxSpeed = speed;
	position = 0;
	type = typeOfAnimal;
	
}

/**
 * @return the maximum speed of the animal
 */
public int getMaxSpeed() {
	return maxSpeed;
}

/**
 * @return the remaining speed of the animal for that turn
 */
public int getTurnSpeed() {
	remainingSpeed = randy.nextInt(maxSpeed) + 1;
	return remainingSpeed;
}

/**
 * @return the current position of the animal
 */
public int getPosition() {
	return position;
}

/**
 * @return the name of the animal
 */
public String getName() {
	return name;
}

/**
 * Is overridden by the subclasses
 * @param racetrack
 */
public void move(char[] racetrack) {
}

/**
 * @return the type of the animal
 */
public String getType() {
	return type;
}
/**
 * @return if the animal is a winner
 */
public boolean flag(char[] raceArray) {
	if(position == raceArray.length) {
		return true;
	}
	return false;
}
}


