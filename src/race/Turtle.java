package race;
import java.util.Random;

public class Turtle extends Animal {
protected static int MAX_SPEED = 2;
protected static String TYPE = "Turtle";
protected String[] nameArray  = {"Kaden", "Finn", "Charger", "Aussie", "Speedy", "Mr. Bean"};
protected String name;
protected Random randy;
public Turtle() {
	super(MAX_SPEED, TYPE);
	randy = new Random();
	name = nameArray[randy.nextInt(6)];
}
/**
 * Moves the Animal along the track based on it's special properties
 * @param raceArray
 */
public void move(char[] raceArray) {
	int turnSpeed = this.getTurnSpeed();
	int currentPosition = this.getPosition();
	while(turnSpeed > 0) {
		if(currentPosition >= raceArray.length-1) {
			currentPosition = raceArray.length - 2;
			turnSpeed = 0;
		}
		
		 if(raceArray[currentPosition+1] == 'O') {
		 turnSpeed++;
		 }
		 if(currentPosition >= raceArray.length-1) {
				currentPosition = raceArray.length - 2;
				turnSpeed = 0;
			}
		currentPosition++;
		turnSpeed--;
	}
	position = currentPosition;
}
public String getName() {
	return name;
}
}